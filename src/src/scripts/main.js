// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'


var breadcrumb = {
    init: function(){
        this.dom();
        this.events();
    },
    dom: function(){
        this.$next = $('#next')
        this.$back = $('#back')
        this.$breadcrumb = $('s-breadcrumbs').find('li')
    },
    events: function(){
        var next = this.$next
        var back = this.$back
        var bread = this.$breadcrumb
        var active = $('.actived')

        next.click(function(){
            active = active.next()
            $('li.actived').removeClass('actived animated bounceIn')
            active.addClass('actived animated bounceIn')
        })

        
        back.click(function(){
            active = active.prev()
            $('li.actived').removeClass('actived animated bounceIn')
            active.addClass('actived animated bounceIn')
        })
    }
}

breadcrumb.init()


// var b = $('li').eq(0)
// var a = $('li').eq(3)

// if(b.hasClass('actived')){
//     $('#back').hide()
// } else if( a.hasClass('actived')){
//     $('#next').hide()
// } else {
//     $('#back').show()
//     $('#next').show()
// }